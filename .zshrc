# users generic .zshrc file for zsh(1)

## Pre required
# install anyanv
# install brew
# install zsh-syntax-highlighting
#   brew install zsh-syntax-highlighting
#
#```
#mkdir ~/zsh_plugin/
#cd ~/zsh_plugin/
#git clone git://github.com/zsh-users/zaw.git
#```


# ------------------------------------------------------------------------
# anyenv
# ------------------------------------------------------------------------
if [ -d $HOME/.anyenv ] ; then
    export PATH="$HOME/.anyenv/bin:$PATH"
    eval "$(anyenv init - zsh)"
fi

# ------------------------------------------------------------------------
# sdkman
# ------------------------------------------------------------------------
source "/home/cimadai/.sdkman/bin/sdkman-init.sh"

## Environment variable configuration
#
# LANG
#
export LANG=ja_JP.UTF-8

export GOPATH=$HOME/GOPATH

# ADD PATH
export PATH=~/sdk/platform-tools:$PATH
export PATH=~/sdk/tools:$PATH
#export PATH=$HOME/Library/Developer/Xamarin/android-sdk-macosx/platform-tools:$PATH
export PATH=~/Library/Android/sdk/ndk-bundle:$PATH
export PATH=/usr/local/opt/llvm/bin/:$PATH
#export PATH=$PATH:/opt/local/bin:/opt/local/sbin
export PATH=$PATH:$HOME/local/bin
export PATH=$PATH:/usr/local/bin
export PATH=$PATH:/usr/sbin:
export PATH=$PATH:/sbin
export PATH=$PATH:~/Env/Python2.6.2/Scripts
export PATH=$PATH:~/Library/Python/2.7/bin
export PATH=$PATH:~/Env/play-1.2
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/usr/local/cuda/bin
export PATH=$PATH:~/.nodebrew/current/bin
export PATH=$PATH:`npm bin -g`
export PATH=$PATH:/Applications/Genymotion\ Shell.app/Contents/MacOS/
export PATH=$PATH:/Applications/Genymotion.app/Contents/MacOS/
export PATH=$PATH:~/Library/Enthought/Canopy_64bit/User/bin
export PATH=$PATH:~/.conscript/bin
export PATH=$PATH:$HOME/env
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin
export PATH=$PATH:$HOME/.local_bin/
export PATH=$PATH:$HOME/flutter/bin
export PATH=$PATH:$HOME/bin
export PATH=$PATH:/usr/local/go/bin
export PATH="/usr/local/opt/avr-gcc@7/bin":$PATH

export DYLD_LIBRARY_PATH=/usr/local/cuda/lib:$DYLD_LIBRARY_PATH
export EDITOR=vim
export HOMEBREW_GITHUB_API_TOKEN=a24dd9f4647eda2d8dce2a375d4df42fe1445741

export NODE_PATH=~/.nodebrew/current/lib/node_modules
export ANDROID_NDK_ROOT=~/Library/Android/sdk/ndk-bundle
export ANDROID_HOME=~/Library/Android/sdk
export ANDROID_SDK_ROOT=~/Library/Android/sdk

#export JAVA_HOME=$(/usr/libexec/java_home -v 1.7)
#export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

export IROHA_HOME=~/src/iroha-build/iroha

export PYTHONPATH="~/.local/lib/python3.6/site-packages:$PYTHONPATH"

## Default shell configuration
#
# set prompt
#
autoload colors
colors

# auto change directory
#
setopt auto_cd

# auto directory pushd that you can get dirs list by cd -[tab]
#
setopt auto_pushd

# command correct edition before each completion attempt
#
setopt correct

# compacked complete list display
#
setopt list_packed

# no remove postfix slash of command line
#
setopt noautoremoveslash

# no beep sound when complete list displayed
#
setopt nolistbeep

## Keybind configuration
#
# vim like keybind 
#
bindkey -v
bindkey -a 'q' push-line
bindkey -a 'H' run-help

# vimモード表示###################################
function vim_mode_echo {
  # エラーが発生したら赤くなる。
  COLORED_PROMPT="%(?.$.%F{red}$%f)"
  case $KEYMAP in
    vicmd)
    echo "%{$fg[red]%}[%{$reset_color%}%{$fg_bold[red]%}NOR%{$reset_color%}%{$fg[red]%}]%{$fg[cyan]%}${COLORED_PROMPT}%{$reset_color%}"
    ;;
    main|viins)
    echo "%{$fg[red]%}[%{$reset_color%}%{$fg_bold[cyan]%}INS%{$reset_color%}%{$fg[red]%}]%{$fg[cyan]%}${COLORED_PROMPT}%{$reset_color%}"
    ;;
  esac
}

# Gitの状態を表示 ############################
autoload -Uz VCS_INFO_get_data_git; VCS_INFO_get_data_git 2> /dev/null
function rprompt-git-current-branch {
    local name st color gitdir action
    if [[ "$PWD" =~ '/\.git(/.*)?$' ]]; then
        return
    fi

    name=`git rev-parse --abbrev-ref=loose HEAD 2> /dev/null`
    if [[ -z $name ]]; then
        return
    fi

    gitdir=`git rev-parse --git-dir 2> /dev/null`
    action=`VCS_INFO_git_getaction "$gitdir"` && action="($action)"

    if [[ -e "$gitdir/rprompt-nostatus" ]]; then
        echo "$name$action "
        return
    fi

    st=`git status 2> /dev/null`
    if [[ -n `echo "$st" | grep "^nothing to"` ]]; then
        color=%F{green}
    elif [[ -n `echo "$st" | grep "^nothing added"` ]]; then
        color=%F{yellow}
    elif [[ -n `echo "$st" | grep "^# Untracked"` ]]; then
        color=%B%F{red}
    else
        color=%F{red}
    fi

    echo "$color$name$action%f%b "
}

function user_info_echo {
    case ${UID} in
    0) # root
        echo "%B%{${fg[blue]}%}[%n@%m]%{${fg[yellow]}%}%(#.#.%%)%{${reset_color}%}%b "
      ;;
    *) # 一般ユーザー
        echo "%{${fg[blue]}%}[%n@%m]%{${fg[yellow]}%}%(#.#.%%)%{${reset_color}%} "
      ;;
    esac
}

function zle-line-init zle-keymap-select {
  case $KEYMAP in
    vicmd)
        PROMPT="$(vim_mode_echo) "
        RPROMPT="%{${fg[green]}%}[$(rprompt-git-current-branch)%{${fg[green]}%}%~]%{${reset_color}%}"
    ;;
    main|viins)
        PROMPT="$(vim_mode_echo) "
        RPROMPT="%{${fg[green]}%}[$(rprompt-git-current-branch)%{${fg[green]}%}%~]%{${reset_color}%}"
    ;;
  esac
  zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select

# 「%(#.#.%%)%」: rootは#、一般ユーザは$
# %n : ユーザー名
# %m : ホスト名
# %d : カレントディレクトリ
# PROMPT : 通常時の左プロンプト
# RPROMPT : 通常時の右プロンプト
# PROMPT2 : for/whileのプロンプト
# SPROMPT : スペルミス時のプロンプト
# %B : start bold
# %b : stop bold
case ${UID} in
0) # root
    PROMPT2="%B%{${fg[blue]}%}%_#%{${reset_color}%}%b "
    SPROMPT="%B%{${fg[blue]}%}%r is correct? [n,y,a,e]:%{${reset_color}%}%b "
  ;;
*) # 一般ユーザー
    PROMPT2="%{${fg[blue]}%}%_#%{${reset_color}%} "
    SPROMPT="%{${fg[blue]}%}%r is correct? [n,y,a,e]:%{${reset_color}%} "
  ;;
esac

# historical backward/forward search with linehead string binded to ^P/^N
#
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^p" history-beginning-search-backward-end
bindkey "^n" history-beginning-search-forward-end
bindkey "\\ep" history-beginning-search-backward-end
bindkey "\\en" history-beginning-search-forward-end

## Command history configuration
#
function mkcd(){mkdir -p $1 && cd $1}
source /home/cimadai/zsh_plugins/zaw/zaw.zsh
HISTFILE=~/.zsh_history
HISTSIZE=50000
SAVEHIST=50000
HISTTIMEFORMAT='%Y/%m/%d %H:%M:%S '
setopt hist_ignore_dups # ignore duplication command history list
setopt share_history # share command history data
function history-all { history -E 1 }

bindkey "^h" zaw-history

# ヒストリに追加されるコマンド行が古いものと同じなら古いものを削除
setopt hist_ignore_all_dups
# 古いコマンドと同じものは無視 
setopt hist_save_no_dups
# historyコマンドは履歴に登録しない
setopt hist_no_store

## Alias configuration
#
# expand aliases before completing
#
setopt complete_aliases # aliased ls needs if file/dir completions work

alias where="command -v"
alias j="jobs -l"

case "${OSTYPE}" in
freebsd*|darwin*)
  alias ls="ls -G -w"
  ;;
linux*)
  alias ls="ls --color"
  ;;
cygwin*)
  alias ls="ls --color=auto"
  ;;
esac

alias la="ls -a"
alias lf="ls -F"
alias ll="ls -la"

alias du="du -h"
alias df="df -h"

alias su="su -l"

alias so="source ~/.zshrc"

alias ipython='ipython --pylab'

alias ctags='/Applications/MacVim.app/Contents/MacOS/ctags "$@"'

alias jadx='~/insync/dicek/Home/tools_java/jadx-0.6.1/bin/jadx-gui'
alias dex2jar="~/insync/dicek/Home/tools_java/dex2jar-2.0/d2j-dex2jar.sh"

alias cmakeclean='find . -type d -exec rm -f "{}/CMakeCache.txt" "{}/cmake_install.cmake" \; -exec rm -rf "{}/CMakeFiles" \; -exec rm -f "{}/Makefile" \;'

function docker-force-rm () { docker stop $1 && docker rm $1 }
function docker-remove-stop-container () { docker rm `docker ps -a -q` }

## terminal configuration
#
unset LSCOLORS
case "${TERM}" in
xterm)
  export TERM=xterm-color
  ;;
kterm)
  export TERM=kterm-color
  # set BackSpace control character
  stty erase
  ;;
cons25)
  unset LANG
  export LSCOLORS=ExFxCxdxBxegedabagacad
  export LS_COLORS='di=01;34:ln=01;35:so=01;32:ex=01;31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
  zstyle ':completion:*' list-colors \
    'di=;34;1' 'ln=;35;1' 'so=;32;1' 'ex=31;1' 'bd=46;34' 'cd=43;34'
  ;;
esac

# set terminal title including current directory
#
case "${TERM}" in
kterm*|xterm*)
  precmd() {
    echo -ne "\033]0;${USER}@${HOST%%.*}:${PWD}\007"
  }
  export LSCOLORS=exfxcxdxbxegedabagacad
  export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
  zstyle ':completion:*' list-colors \
    'di=34' 'ln=35' 'so=32' 'ex=31' 'bd=46;34' 'cd=43;34'
  ;;
esac

# screen用設定
alias s='screen -U'

## for brew-cask
export HOMEBREW_CASK_OPTS="--appdir=/Applications"

## Completion configuration
#
fpath=(/home/cimadai/zsh_plugins/zsh-completions $fpath)

## Syntax highlighting
source /home/cimadai/zsh_plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

autoload -U compinit
compinit

###-tns-completion-start-###
if [ -f ~/.tnsrc ]; then 
    source ~./tnsrc 
fi
###-tns-completion-end-###

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# google_translate [-j, -e]
# $ google_translate -e power line
function google_translate() {
  local str opt arg

    str=`pbpaste` # clipboard
    arg=`echo ${@:2} | sed -e 's/  */+/g'` # argument
    en_jp="?hl=ja&sl=en&tl=ja&ie=UTF-8&oe=UTF-8" # url

    case "$1" in
        "-j") opt="?hl=ja&sl=ja&tl=en&ie=UTF-8&oe=UTF-8&text=${arg}";; # jp -> en translate
        "-e") opt="${en_jp}&text=${arg}";; # en -> jp translate
        *) opt="${en_jp}&text=${str}";; # en -> jp translate
    esac

  print w3m +20 "http://translate.google.com/${opt}"  # goto 20 line
  w3m +20 "http://translate.google.com/${opt}"  # goto 20 line
}

setopt no_global_rcs

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
#export SDKMAN_DIR="/Users/daisuke-shimada/.sdkman"
#[[ -s "/Users/daisuke-shimada/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/daisuke-shimada/.sdkman/bin/sdkman-init.sh"
#

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

eval $(/mnt/c/Users/cimad/tools/ssh-agent-wsl/ssh-agent-wsl -r)

## >>>>> Deno
export DENO_INSTALL="/home/cimadai/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

fpath=(~/.zsh $fpath)
autoload -Uz compinit
compinit -u
## <<<<<< Deno

## >>>>> brew linux
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/cimadai/.zprofile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
## <<<<< brew linux
#
#
export PATH="/home/cimadai/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
