#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0);pwd)

ln -f -s $SCRIPT_DIR/.gitconfig $HOME/.gitconfig
ln -f -s $SCRIPT_DIR/.gvimrc $HOME/.gvimrc
ln -f -s $SCRIPT_DIR/.jslintrc $HOME/.jslintrc
ln -f -s $SCRIPT_DIR/.rst2pdf $HOME/.rst2pdf
ln -f -s $SCRIPT_DIR/.screenrc $HOME/.screenrc
ln -f -s $SCRIPT_DIR/.vimrc $HOME/.vimrc
ln -f -s $SCRIPT_DIR/.zshrc $HOME/.zshrc
ln -f -s $SCRIPT_DIR/.gitmessage $HOME/.gitmessage
ln -f -s $SCRIPT_DIR/.ideavimrc $HOME/.ideavimrc

mkdir -p $HOME/.config/karabiner
ln -f -s $SCRIPT_DIR/karabiner/karabiner.json $HOME/.config/karabiner/karabiner.json
