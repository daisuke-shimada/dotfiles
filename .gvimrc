" vim:set ts=4 sts=4 sw=4 tw=0 fdm=marker:

" メニューを強制的に utf-8 にする"{{{
source $VIMRUNTIME/delmenu.vim
set langmenu=ja_JP.utf-8.vim
source $VIMRUNTIME/menu.vim
"}}}

" フォント設定:"{{{
if has('win32')
  " Windows用
  set guifont=MotoyaLMaru:h12:cSHIFTJIS
  " 行間隔の設定
  set linespace=1
  " 一部のUCS文字の幅を自動計測して決める
  if has('kaoriya')
    set ambiwidth=auto
  endif
elseif has('mac')
  set guifont=Monaco:h12
elseif has('xfontset')
  " UNIX用 (xfontsetを使用)
  set guifontset=a14,r14,k14
endif
"}}}

" ウインドウに関する設定:"{{{
set columns=160                 " ウインドウの幅
set lines=45                    " ウインドウの高さ
" autocmd GUIEnter * simalt ~x    " 起動時に最大化する
winpos 0 0                      " 起動時のWindowの位置(:winposで現在の位置が取得できる)
"}}}


set t_vb=                       " カーソルが一番上や下に移動した時のビープ音を消す＆画面フラッシュも消す
"gui
"set transparency=230

" 色設定 "{{{
set t_Co=256
colorscheme birds-of-paradise
"}}}

set imdisable

" Window位置の記憶 {{{
let g:save_window_file = expand('~/.vimwinpos')
augroup SaveWindow
  autocmd!
  autocmd VimLeavePre * call s:save_window()
  function! s:save_window()
    let options = [
      \ 'set columns=' . &columns,
      \ 'set lines=' . &lines,
      \ 'winpos ' . getwinposx() . ' ' . getwinposy(),
      \ ]
    call writefile(options, g:save_window_file)
  endfunction
augroup END

if filereadable(g:save_window_file)
  execute 'source' g:save_window_file
endif
"}}}

" __END__
" vim: set foldmethod=marker:"
