" vim:set ts=4 sts=4 sw=4 tw=0 fdm=marker:
"
" How to setup:
"
" # create bundle directory
" mkdir -p ~/.vim/bundle
" # clone NeoBundle from repository
" git clone git://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim
" # Then restart vim and install plugins following prompt.

set modeline
set nocompatible
filetype off                   " required!
let $TMP=$HOME. "/tmp"
set viminfo+=n$TMP/.viminfo
set runtimepath^=$HOME/.vim/plugin/qfixhowm,$HOME/.vim/,$HOME/.vim/after/

" Leader -> , {{{
let mapleader = ","
" ,のデフォルトの機能は、\で使えるように退避
noremap \  ,
"}}}

"---------------------------
" Start Neobundle Settings. {{{
"---------------------------
" bundleで管理するディレクトリを指定
set runtimepath+=~/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" neobundle自体をneobundleで管理
NeoBundleFetch 'Shougo/neobundle.vim'

" NeoBundle 'Shougo/neocomplcache'

" NeoBundle 'scrooloose/syntastic'

NeoBundle 'vim-scripts/zoom.vim'

""" 辞書
NeoBundle 'mattn/excitetranslate-vim'

""" over.vim {{{
"NeoBundle 'osyo-manga/vim-over'
"    " over.vimの起動
"    nnoremap <silent> <Leader>l :OverCommandLine<CR>
"    " カーソル下の単語をハイライト付きで置換
"    nnoremap sub :OverCommandLine<CR>%s/<C-r><C-w>//g<Left><Left>
"    " コピーした文字列をハイライト付きで置換
"    nnoremap subp y:OverCommandLine<CR>%s!<C-r>=substitute(@0, '!', '\\!', 'g')<CR>!!gI<Left><Left><Left>
"" }}}
"
"NeoBundle 'itchyny/lightline.vim'

" nerdtree {{{
NeoBundle 'scrooloose/nerdtree'
"for The-NERD-Commenter
" Nerd_Commenter の基本設定
let g:NERDCreateDefaultMappings = 0
let NERDSpaceDelims = 1
nmap <Leader>/ <Plug>NERDCommenterToggle
vmap <Leader>/ <Plug>NERDCommenterToggle
" }}}

"NeoBundle 'Townk/vim-autoclose'

" markdown {{{
NeoBundle 'godlygeek/tabular'
NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'kannokanno/previm'
NeoBundle 'tyru/open-browser.vim'
au BufRead,BufNewFile *.md set filetype=markdown
"}}}

" yaml {{{
NeoBundle 'mrk21/yaml-vim'
"}}}

" memolist {{{
NeoBundle 'fuenor/qfixgrep.git'
NeoBundle 'glidenote/memolist.vim'
let g:memolist_qfixgrep = 1
map <Leader>mn  :MemoNew<CR>
map <Leader>ml  :MemoList<CR>
map <Leader>mg  :MemoGrep<CR>
" memolist }}}
"
" カラースキーム一覧表示に Unite.vim を使う {{{
NeoBundle 'Shougo/unite.vim'
NeoBundle 'ujihisa/unite-colorscheme'
"}}}

NeoBundle 'ronny/birds-of-paradise.vim'

NeoBundle 'tomlion/vim-solidity'

call neobundle#end()

" Required:
filetype plugin indent on

" 未インストールのプラグインがある場合、インストールするかどうかを尋ねてくれるようにする設定
" 毎回聞かれると邪魔な場合もあるので、この設定は任意です。
NeoBundleCheck

"-------------------------
" End Neobundle Settings.
"-------------------------


"for neocomplecache
let g:neocomplcache_enable_at_startup = 0
imap <C-k> <Plug>(neocomplcache_snippets_expand)
smap <C-k> <Plug>(neocomplcache_snippets_expand)

" sorround.vim のコマンドたち"{{{
"<tag>"hoge"</tag>
" ds : 周辺の削除　(ex)ds" , ds{, dst
" cs : 周辺の置換　(ex)cs"', cst<newtag>
" ys : 周辺への追加　(es)ysiw", ysit<tag>
" yss: カレントラインに対するys
" yS : インデントするys
" ySS: インデントするyss
" 参考(http://project-p.jp/halt/kinowiki/vim/plugin/surround)
"}}}

"}}}

" 折り畳みコマンドたち"{{{
" zi : 折り畳み有効無効の切り替え
" zf : 範囲選択で折り畳みの作成
" za : 折り畳みの開閉
" zd : 折り畳みの削除
" zA : 折り畳みの開閉(再帰)
" zD : 折り畳みの削除(再帰)
" zE : 全ての折り畳みの削除
" zR : 全ての折り畳みを開く
" zM : 全ての折り畳みを閉じる
"}}}

"おまじない系"{{{
set nocompatible
filetype on
filetype plugin indent on   " ファイル別 plugin (~/.vim/ftplugin/拡張子.vim)
"}}}

" メニューを強制的に utf-8 にする"{{{
source $VIMRUNTIME/delmenu.vim
set langmenu=ja_JP.utf-8.vim
source $VIMRUNTIME/menu.vim
"}}}

" 各種設定"{{{
" インデント"{{{
set smartindent     " インデント調整あり、コメント維持
set shiftwidth=4    " tab 文字の入力幅
set tabstop=4       " tab 文字の表示幅
set expandtab       " tab を空白文字に置き換え

"python
autocmd FileType python setl smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd FileType python setl tabstop=2 expandtab shiftwidth=2 softtabstop=2

"}}}

" 削除"{{{
set backspace=indent,eol,start  " BS で indent,改行,挿入開始前を削除
set smarttab        " BS でインデント幅を削除
"}}}

" 検索"{{{
set hlsearch        " 検索文字列を色づけ
set ignorecase      " 大文字小文字を判別しない
set smartcase       " でも大文字小文字が混ざって入力されたら区別する
set incsearch       " インクリメンタルサーチ
set wrapscan        " 検索時にファイルの最後まで行ったら最初に戻る
set wildignore+=*.dll,*.class,*.stamp,*.jar,*.jpg,*.jpeg,*.gif,*.png,*._,**/obj/*
"}}}

" 表示"{{{
set number          " 行番号表示
set ruler           " ルーラを表示
set showcmd         " 入力中のコマンド（キー）を右下に表示
set wildmenu        " 入力中のタブ補完を強化
set wildmode=list:longest,full " 入力補完の設定（リスト表示で最長一致、その後選択）
set showmatch       " 括弧入力で対応する括弧を一瞬強調
set showtabline=2   " タブを常に表示
"タブの左側にカーソル表示
set listchars=tab:\^\ 
set list
"}}}

" その他"{{{
set mouse=a                 " ターミナルでマウスを使う (コピーしたくなったら set mouse=)
set hidden                  " 編集中に他ファイルを開ける (しかも undo 履歴が消えない)
set formatoptions=l         " テキスト挿入中の自動折り返しを無効
set nowrap                  " 長い行を折り返して表示
set laststatus=2            " 常にステータス行を表示
set showcmd                 " コマンドをステータス行に表
set notitle                 " タイトルバーの表示を消す
set hidden                  " バッファが編集中でもその他のファイルを開けるように
set autoread                " 外部のエディタで編集中のファイルが変更されたら自動で読み直す
set clipboard=unnamed       " ヤンク/pでクリップボード操作
set browsedir=buffer        " :browse の初期ディレクトリ(vimエディタで開いているファイルと同じディレクトリ)
set iminsert=1
set guicursor=a:blinkon0        " カーソルを点滅させない
set guioptions-=T               " メニューバー/ツールバー消す
"set guioptions-=m
set shortmess+=I                "起動時のメッセージを消す
set cmdheight=2                 " コマンドラインの高さ(GUI使用時)
set statusline=%<%f\ %m%r%h%w\ [ff:%{&ff}]\ [ft:%{&ft}]\ [fenc:%{&fenc}]\ [pos:%l,%v][%p%%]\ [lines:%L]
" メニューファイルが存在しない場合は予め'guioptions'を調整しておく 
if 1 && !filereadable($VIMRUNTIME . '/menu.vim') && has('gui_running')
  set guioptions-=M
endif
"}}}

" ファイル名に大文字小文字の区別がないシステム用の設定: "{{{
if filereadable($HOME . '/vimrc') && filereadable($HOME . '/ViMrC')
  " tagsファイルの重複防止
  set tags=$HOME/tags/javascript.tags
endif
"}}}

" プラットホーム依存の特別な設定 "{{{
" WinではPATHに$HOMEが含まれていないときにexeを見つけ出せないので修正 
if has('win32') && $PATH !~? '\(^\|;\)' . escape($HOME, '\\') . '\(;\|$\)'
    let $PATH = $HOME . ';' . $PATH
endif
" Macではデフォルトの'iskeyword'がcp932に対応しきれていないので修正 
if has('mac')
    set iskeyword=@,48-57,_,128-167,224-235
endif
"}}}

" 日本語入力に関する設定:"{{{
if has('multi_byte_ime') || has('xim')
  " IME ON時のカーソルの色を設定
  highlight CursorIM guibg=Green guifg=NONE
  " 挿入モード・検索モードでのデフォルトのIME状態設定
  set iminsert=0 imsearch=0
  if has('xim') && has('GUI_GTK')
    " XIMの入力開始キーを設定:
    " 下記の s-space はShift+Spaceの意味でkinput2+canna用設定
    "set imactivatekey=s-space
  endif
  " 挿入モードでのIME状態を記憶させない
  inoremap <silent> <ESC> <ESC>:set iminsert=0<CR>
endif
"}}}

"}}}

" 文字コード関係 "{{{
" from ずんwiki(http://www.kawaz.jp/pukiwiki/?vim#cb691f26)
if &encoding !=# 'utf-8'
    "set encoding=utf-8
    "set fileencoding=utf-8
endif
if has('iconv')
    let s:enc_euc = 'euc-jp'
    let s:enc_jis = 'iso-2022-jp'
    " iconvがeucJP-msに対応しているかをチェック
    if iconv("\x87\x64\x87\x6a", 'cp932', 'eucjp-ms') ==# "\xad\xc5\xad\xcb"
        let s:enc_euc = 'eucjp-ms'
        let s:enc_jis = 'iso-2022-jp-3'
        " iconvがJISX0213に対応しているかをチェック
    elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# "\xad\xc5\xad\xcb"
        let s:enc_euc = 'euc-jisx0213'
        let s:enc_jis = 'iso-2022-jp-3'
    endif
    " fileencodingsを構築
    if &encoding ==# 'utf-8'
        let s:fileencodings_default = &fileencodings
        let &fileencodings = s:enc_jis .','. s:enc_euc .',cp932'
        let &fileencodings = &fileencodings .','. s:fileencodings_default
        unlet s:fileencodings_default
    else
        let &fileencodings = &fileencodings .','. s:enc_jis
        set fileencodings+=utf-8,ucs-2le,ucs-2
        if &encoding =~# '^\(euc-jp\|euc-jisx0213\|eucjp-ms\)$'
            set fileencodings+=cp932
            set fileencodings-=euc-jp
            set fileencodings-=euc-jisx0213
            set fileencodings-=eucjp-ms
            let &encoding = s:enc_euc
            let &fileencoding = s:enc_euc
        else
            let &fileencodings = &fileencodings .','. s:enc_euc
        endif
    endif
    " 定数を処分
    unlet s:enc_euc
    unlet s:enc_jis
endif
" 日本語を含まない場合は fileencoding に encoding を使うようにする 
if has('autocmd')
  function! AU_ReCheck_FENC()
    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
      let &fileencoding=&encoding
    endif
  endfunction
  autocmd BufReadPost * call AU_ReCheck_FENC()
endif
" 改行コードの自動認識 
set fileformats=unix,dos,mac
" □とか○の文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif
"}}}

" スペース、タブの可視化"{{{
" 全角スペース、末尾の半角スペース、タブを色づけする(色は好み、でも全角スペースを□にしたい) 
if has("syntax")
    syntax on
    function! ActivateInvisibleIndicator()
        syntax match InvisibleJISX0208Space "　" display containedin=ALL
        highlight InvisibleJISX0208Space term=underline ctermbg=Blue guifg=#999999 gui=underline
        syntax match InvisibleTrailedSpace "[ ]\+$" display containedin=ALL
        highlight InvisibleTrailedSpace term=underline ctermbg=Red guifg=#FF5555 gui=underline
        syntax match InvisibleTab "\t" display containedin=ALL
        highlight InvisibleTab term=underline ctermbg=Cyan guibg=#333333
    endf

    augroup invisible
        autocmd! invisible
        autocmd BufNew,BufRead * call ActivateInvisibleIndicator()
    augroup END
endif

set textwidth=0
if exists('&colorcolumn')
    set colorcolumn=+1
    " sh,cpp,perl,vim,...の部分は自分が使う
    " プログラミング言語のfiletypeに合わせてください
    autocmd FileType sh,cpp,perl,vim,ruby,python,javascript,scala,rst setlocal textwidth=110
    autocmd FileType text setlocal textwidth=90
    autocmd FileType text,sh,cpp,perl,vim,ruby,python,javascript,scala,rst hi ColorColumn guibg=#888888
endif
"}}}

" マッピング"{{{
"表示行単位で行移動する"{{{
" VsVimのためにコメントアウト
"nmap j gj
"nmap k gk
"vmap j gj
"vmap k gk
"}}}

" GUIオプション"{{{
set mouse=a                     " mousefo" どのモードでもマウスを使えるようにする
set nomousefocus                " マウスの移動でフォーカスを自動的に切替えない
set mousehide                   " 入力時にマウスポインタを隠す)
set equalalways                 " 新規ウィンドウが同じ大きさ
set linespace=4                 " 行間
set cmdheight=2                 " コマンドラインの高さ(GUI使用時)
set statusline=%<%f\ %m%r%h%w\ [ff:%{&ff}]\ [ft:%{&ft}]\ [fenc:%{&fenc}]\ [pos:%l,%v][%p%%]\ [lines:%L]
set vb t_vb=                    " カーソルが一番上や下に移動した時のビープ音を消す＆画面フラッシュも消す
set guicursor=a:blinkon0        " カーソルを点滅させない
"}}}


" 開いているファイルの文字コードを指定して開きなおす{{{
nmap ,ee :e ++enc=euc-jp<CR>
nmap ,es :e ++enc=cp932<CR>
nmap ,ej :e ++enc=iso-2022-jp<CR>
nmap ,eu :e ++enc=utf-8<CR>
nmap ,el :e ++enc=iso-8859-1<CR>

nnoremap <space>w :<C-u>write<Cr>
nnoremap <space>q :<C-u>quit<Cr>
nnoremap <space>v :<C-u>e<space>$HOME/.vimrc
nnoremap <space>g :<C-u>e<space>$HOME/.gvimrc
nnoremap <space>k :<C-u>e<space>$HOME/Library/Application\ Support/Karabiner/private.xml
"}}}

" insert mode時にc-jで抜ける{{{
map <C-j> <esc>
imap <C-j> <esc>
nmap <C-j> <esc>
cmap <C-j> <esc>
"}}}

"スプリットの移動"{{{
nmap wh  <C-w>h
nmap wj  <C-w>j
nmap wk  <C-w>k
nmap wl  <C-w>l
"}}}

"タブ"{{{
nmap ,t :tabnew<CR>
nmap <C-h> :tabp<CR>
nmap <C-l> :tabn<CR>
"}}}

"TagList"{{{
nmap <C-t> :TlistToggle<CR>
"}}}

"SrcExplorer"{{{
nmap <C-s> :SrcExplToggle<CR>
"}}}

" 窓の幅
nmap ,wl <C-w>5>
nmap ,wh <C-w>5<
nmap ,hl <C-w>5+
nmap ,hh <C-w>5-
nmap ,sa <C-w>=

"" Shell settings.
"if has('win32')
"    " Use NYACUS.
"    set shell=/home/tools/nyacus-2.31_2/nyacus.exe
"    set shellcmdflag=-e
"    set shellpipe=\|&\ tee
"    set shellredir=>%s\ 2>&1
"    set shellxquote=\"
"endif

" 矩形選択で自由に移動する Hack#125
set virtualedit+=block

"}}}

"autodate"{{{
let autodate_format='%Y/%m/%d'"}}}

"自動でcopen"{{{
au QuickfixCmdPost make,grep,grepadd,vimgrep copen"}}}

" Tempディレクトリ"{{{
let $TMP_DIR=$TMP. "/vim_tmp"
if( !isdirectory(expand($TMP_DIR)."/vim_view") )
    call mkdir(expand($TMP_DIR)."/vim_view","p")
endif

if( !isdirectory(expand($TMP_DIR)."/vim_swp") )
    call mkdir(expand($TMP_DIR)."/vim_swp","p")
endif

if( !isdirectory(expand($TMP_DIR)."/vim_backup") )
    call mkdir(expand($TMP_DIR)."/vim_backup","p")
endif"}}}

if( !isdirectory(expand($TMP_DIR)."/vim_undo") )
    call mkdir(expand($TMP_DIR)."/vim_undo","p")
endif"}}}

" バックアップ"{{{
set backup
set backupdir=$TMP_DIR/vim_backup
"}}}

" スワップ"{{{
set swapfile
set directory=$TMP_DIR/vim_swp
"}}}

" folding 保存"{{{
set viewoptions=folds,cursor
set viewdir=$TMP_DIR/vim_view
au BufWritePost * mkview
au BufReadPost  * loadview
"}}}

" undo {{{
set undodir=$TMP_DIR/vim_undo
" }}}

" Esc連打でnohilite"{{{
nmap <Esc><Esc> :noh<CR>"}}}

" omnifunc"{{{
setlocal omnifunc=syntaxcomplete#Complete
let g:acp_behaviorFileLength=4"}}}

" rails.vim"{{{
let g:rails_level=4
let g:rails_default_database="mysql"
let g:rails_dbext=1
let g:rails_expensive=1"}}}

" rubycomplete.vim"{{{
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1
autocmd FileType php :set sw=2 expandtab"}}}

"Rubyのオムニ補完を設定(ft-ruby-omni)"{{{
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1
let g:rubycomplete_rails = 1"}}}

"project.vimの幅"{{{
let g:proj_window_width=60"}}}

"lightline.vim"{{{"{{{
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'filename' ] ]
      \ },
      \ 'component_function': {
      \   'fugitive': 'MyFugitive',
      \   'readonly': 'MyReadonly',
      \   'modified': 'MyModified',
      \   'filename': 'MyFilename'
      \ },
      \ 'separator': { 'left': '>', 'right': '<' },
      \ 'subseparator': { 'left': '>', 'right': '<' }
      \ }

function! MyModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

function! MyReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return "?"
  else
    return ""
  endif
endfunction

function! MyFugitive()
  return exists('*fugitive#head') ? fugitive#head() : ''
endfunction

function! MyFilename()
  return ('' != MyReadonly() ? MyReadonly() . ' ' : '') .
       \ ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
       \ ('' != MyModified() ? ' ' . MyModified() : '')
endfunction"}}}"}}}

" unite-outline {{{
nnoremap <silent> <Leader>o :<C-u>Unite -vertical -no-quit outline<CR>
" unite-outline }}}

" 色設定 "{{{
set t_Co=256
syntax enable
colorscheme birds-of-paradise
:hi LineNr ctermfg=239 ctermbg=None
"}}}

" アンダーライン"{{{
set cursorline
hi CursorLine cterm=underline gui=underline ctermbg=NONE guibg=NONE
"}}}

" office {{{
augroup office_format
    autocmd!
    autocmd BufEnter *.{docx,xlsx,pptx,ppt,doc,xls}  set modifiable
    autocmd BufEnter *.{docx,xlsx,pptx,ppt,doc,xls}  silent %d
    autocmd BufEnter *.{docx,xlsx,pptx,ppt,doc,xls}  silent %read !tika --text %:p 
    autocmd BufEnter *.{docx,xlsx,pptx,ppt,doc,xls}  set readonly
augroup END
" }}}

nmap <C-t> :tabnew<CR>
nmap <C-n> :tabnext<CR>
nmap <C-p> :tabprevious<CR>

" Don't screw up folds when inserting text that might affect them, until
" leaving insert mode. Foldmethod is local to the window. Protect against
" screwing up folding when switching between windows.
autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif

" __END__
" vim: set foldmethod=marker"}}}
